﻿using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace TimeTrees
{
    struct TimelineEvent
    {
        public DateTime dateEvent;
        public string Event;
    }

    struct Person
    {
         public int id;
         public string name;
         public DateTime Birthday;
         public DateTime Death;   
    }
    class Program
    {
        const int nameIndex = 2;
        const int deathIndex = 3;
        const int birthdayIndex = 1;
        const int idIndex = 0;

        static void Main(string[] args)

        {
            Console.WriteLine("Введите формат файла csv или json:");
            string formatFile = Console.ReadLine();

            Person[] person = FormatPeople(formatFile);
            TimelineEvent[] timelineEvent = FormatTimeline(formatFile);

            DiffMinMaxDateId(timelineEvent);

            CheckLeapYear(person);
        }


        static (int, int, int) DiffMinMaxDate(TimelineEvent[] timeline)
        {
            (DateTime minDate, DateTime maxDate) = MinMaxDate(timeline);

            return (maxDate.Year - minDate.Year, maxDate.Month - minDate.Month, maxDate.Day - minDate.Day);
        }
        static DateTime ParseDate(string dateFile)
        {
            DateTime date;

            if (!DateTime.TryParseExact(dateFile, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(dateFile, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(dateFile, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        date = default;
                    }
                }
            }
            return date;
        }



        static (DateTime, DateTime) MinMaxDate(TimelineEvent[] timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;

            for (int i = 0; i < timeline.Length; i++)
            {
                DateTime date = timeline[i].dateEvent;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }
            return (minDate, maxDate);
        }

        static TimelineEvent[] ParseTimeline(string[] timeline)
        {
            TimelineEvent[] timelineEvent = new TimelineEvent[timeline.Length];
            object[][] split = new string[timeline.Length][];

            for (var i = 0; i < timeline.Length; i++)
            {
                var line = timeline[i];
                object[] parts = line.Split(";");

                timelineEvent[i].dateEvent = ParseDate(parts[0].ToString());
                timelineEvent[i].Event = parts[1].ToString();
            }
            return timelineEvent;
        }

        static Person[] ParsePeople(string[] people)
        {
            Person[] person = new Person[people.Length];
            object[][] split = new object[people.Length][];

            for (var i = 0; i < people.Length; i++)
            {
                var line = people[i];
                object[] parts = line.Split(";");

                person[i].id = int.Parse(parts[idIndex].ToString());
                person[i].Birthday = ParseDate(parts[birthdayIndex].ToString());
                person[i].name = parts[nameIndex].ToString();

                if (parts.Length == 4)
                {
                    person[i].Death = ParseDate(parts[deathIndex].ToString());
                }
                else
                {
                    person[i].Death = default;
                }
            }
            return person;
        }

        static void CheckLeapYear(Person[] people)
        {
            for (var i = 0; i < people.Length; i++)
            {
                DateTime lastDate = people[i].Birthday;
                DateTime newDate = DateTime.Now;

                if (people[i].Death != default)
                {
                    newDate = people[i].Death;
                }
                if (DateTime.IsLeapYear(lastDate.Year) && (lastDate.Year - newDate.Year <= 20))
                {
                    Console.WriteLine($"{people[i].name} родился в високосный год и его возраст не более 20 лет");
                }
            }
        }

        static void DiffMinMaxDateId(TimelineEvent[] timelineEvent)
        {
            (int year, int month, int day) = DiffMinMaxDate(timelineEvent);
            Console.WriteLine($"Между максимальной и минимальной датой прошло: {year} лет, {month} месяцев и {day} дней");
        }

        static Person[] FormatPeople(string formatFile)
        {
            Person[] person = default;

            if (formatFile == "csv")
            {
                string[] people = File.ReadAllLines($"people.{formatFile}");
                Person[] peopleCSV = ParsePeople(people);
                person = peopleCSV;
            }
            if (formatFile == "json")
            {
                string json = File.ReadAllText($"people.{formatFile}");
                Person[] peopleJSON = JsonConvert.DeserializeObject<Person[]>(json);
                person = peopleJSON;
            }
            return person;

        }

        static TimelineEvent[] FormatTimeline(string formatFile)
        {
            TimelineEvent[] timelineEvent = default;

            if (formatFile == "csv")
            {
                string[] timeline = File.ReadAllLines($"timeline.{formatFile}");
                TimelineEvent[] timelineCSV = ParseTimeline(timeline);
                timelineEvent = timelineCSV;
            }
            if (formatFile == "json")
            {
                string json = File.ReadAllText($"people.{formatFile}");
                TimelineEvent[] timelineJSON = JsonConvert.DeserializeObject<TimelineEvent[]>(json);
                timelineEvent = timelineJSON;
            }
            return timelineEvent;
        }

    }
}
